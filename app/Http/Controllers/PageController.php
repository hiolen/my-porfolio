<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Routing\ResponseFactory;
use Mail;

class PageController extends Controller
{
		protected $response;

		public function __construct(ResponseFactory $response) {
			$this->response = $response;
		}

    public function index(){
    	return view('portfolio.index');
    }

    public function contact(Request $request){
    	if ($request->ajax()){
    		$data = array(
	    		'name' => $request->name,
	    		'website' => $request->website,
	    		'email' => $request->email,
	    		'text' => $request->message
    		);

    		Mail::send('emails.contact', $data, function ($m) {

		        $m->from('rodel.hiolen@gmail.com', 'Portfolio Contact');

		        $m->to('rodel.hiolen@gmail.com')->subject('Portfolio Contact');

		    });

    		$response = array(
    			'message' => 'Your email has been sent successfully!',
    			'data' => $request->email
    		);

    		return $this->response->json($response);
    	}
    }
}
