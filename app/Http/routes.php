<?php

Route::get('/', ['as' => 'page.index', 'uses' => 'PageController@index']);
Route::get('/contact', ['as' => 'page.contact', 'uses' => 'PageController@contact']);

// Send Email
Route::get('sendemail', function () {

    $data = array(
        'name' => "Learning Laravel",
    );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('rodel.hiolen@gmail.com', 'Learning Laravel');

        $message->to('rodel.hiolen@gmail.com')->subject('Learning Laravel test email');

    });

    return "Your email has been sent successfully";

});