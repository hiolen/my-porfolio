@extends('portfolio.layouts.app')

@section('main_content')
    
    <!-- navbar -->
    @include('portfolio.partials.navbar') 

    <!-- about -->
    @include('portfolio.partials.about')

    <!-- design cover -->
    @include('portfolio.partials.design-cover')

    <!-- work section -->
    @include('portfolio.partials.work-section')

    <!-- working process -->
    @include('portfolio.partials.working-process')

    <!-- portfolio section -->
    @include('portfolio.partials.portfolio-section')

    <!-- social share section -->
    @include('portfolio.partials.social-share-section')

    <!-- fun facts section -->
    @include('portfolio.partials.fun-facts-section')

    <!-- contact section -->
    @include('portfolio.partials.client-section')

    <!-- footer -->
    @include('portfolio.partials.footer')
@endsection

@section('extra_script')
<script type="text/javascript">
    $(function() {
        $('#formContact').on('submit', function(e) {
            e.preventDefault();

            var token = '{{ csrf_token() }}';
            var name = $('#contact-name').val();
            var website = $('#contact-website').val();
            var email = $('#contact-email').val();
            var message = $('#message').val();

            if ($('#formContact').valid()){
                $.get("{{ route('page.contact') }}",{_token:token, name:name, website:website, email:email, message:message}, function(data) {
                    console.log(data);

                    var message = '<p class="alert-section">' +
                    '<div class="alert-msg">' +
                        '<div class="alert alert-success">' +
                            '<p class="text-center"><strong>' + data.message + '</strong></p>' +
                        '</div>' +
                        '</div>'+
                    '</p>';

                    $('#alert-section').replaceWith(message);

                    // Fade out alert
                      $(".alert-msg").fadeTo(2000, 500).slideUp(500, function(){
                        $(".alert-msg").slideUp(500);
                        $(".alert-msg").remove();
                      });
                    // reset form
                    $('#formContact').trigger("reset");
                });
            }else
                return false;
        });

        // Validate Add Page
        $('#formContact').validate({
              rules: {
                  name: {
                      minlength: 3,
                      maxlength: 55,
                      required: true
                  },
                  email: {
                      required: true,
                      email: true
                  },
                  website: {
                      required: true
                  },
                  message: {
                      minlength: 5,
                      maxlength: 1500,
                      required: true                     
                  },
              },
              highlight: function(element) {
                  $(element).closest('.form-group').addClass('has-error');
              },
              unhighlight: function(element) {
                  $(element).closest('.form-group').removeClass('has-error');
              },
              errorElement: 'span',
              errorClass: 'help-block',
              errorPlacement: function(error, element) {
                  if (element.parent('.input-group').length) {
                      error.insertAfter(element.parent());
                  } else {
                      error.insertAfter(element);
                  }
              },
              submitHandler: function(form) {
                  // leave it blank here.
              }
        });   
    });
</script>
@endsection