<section class="working-prosess">
    <div class="container">
        <div class="row">
            <div class="working-item wow swing center animated">
                <i class="fa fa-ioxhost"></i>
                <h2>Idea</h2>
            </div><!-- end of /.working item 2 -->
            <div class="working-item wow swing center animated">
                <i class="fa fa-github-alt"></i>
                <h2>Plan</h2>
            </div><!-- end of /.working item 1 -->
            <div class="working-item wow swing center animated">
                <i class="fa fa-pencil-square-o"></i>
                <h2>Sketch  </h2>
            </div><!-- end of /.working item 3 -->
            <div class="working-item wow swing center animated">
                <i class="fa fa-desktop"></i>
                <h2>Code</h2>
            </div><!-- end of /.working item 4 -->
            <div class="working-item wow swing center animated">
                <i class="fa fa-check"></i>
                <h2>Launch</h2>
            </div><!-- end of /.working item 5 -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of working prosess /.section -->