<section class="social-share-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Interested in more details? find me on Behance, Dribble</h3>
                <a href="#">
                    <i class="fa fa-behance"></i>&nbsp; Behance
                </a>
                <a href="#">
                    <i class="fa fa-dribbble"></i>&nbsp; Dribbble
                </a>
            </div><!-- end of /. column -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of /.social share -->