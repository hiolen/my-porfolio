<section class="work-section smoth" id="service">
    <div class="container">
        <div class="row">
            <div class="section-title wow bounceInUp center animated">
                <h2>MY WORKING AREA</h2><!-- section title -->
            </div>
            <div class="col-lg-6 mobile">
                <article class="mobile-design work-box wow bounceInDown center animated">
                    <i class="fa fa-align-justify"></i><!-- icon -->
                    <h3>Framework</h3><!-- title -->
                    <p>Libraries of server-side programming languages that construct the back-end structure of a site.</p><!-- caption -->
                    <div class="dh-overlay"></div>
                </article>

            </div><!-- end of /.columns 1 -->
            <div class="col-lg-6 template ">
                <article class="template-design work-box wow bounceInDown center animated">
                    <i class="fa fa-stack-overflow"></i><!-- icon -->
                    <h3>Stack</h3><!-- title -->
                    <p>Comprises the database, servier-side framework, server, and operating system.</p><!-- caption -->
                    <div class="dh-overlay"></div>
                </article>

            </div><!-- end of /.columns 1 -->

            <div class="col-lg-12 work-center-box">
                <div class="main-center-item">
                    <span></span>
                </div>
            </div><!--end of main center columns-->
            <div class="col-lg-6 ui">
                <article class="ui-design work-box wow bounceInUp center animated">
                    <i class="fa fa-ils"></i><!-- icon -->
                    <h3>APIs</h3><!-- title -->
                    <p>Data is exchanged between a database and any software accessing it.</p><!-- caption -->
                    <div class="dh-overlay"></div>
                </article><!-- end of /.design box -->

            </div><!-- end columns 1 -->
            <div class="col-lg-6 landing wow bounceInUp center animated">
                <article class="landing-page work-box">
                    <i class="fa fa-clipboard dh-container"></i><!-- icon -->
                    <h3>LAMP: Linux/Apache/MySQL/PHP</h3><!-- title -->
                    <p>LAMP benefits: flexible, customizable, easy to develop, easy to deploy, secure, and comes with a huge support community since it’s open source.</p><!-- caption -->
                    <div class="dh-overlay"></div>
                </article><!-- end of /.design box -->

            </div><!-- end columns 1 -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of work/.section -->