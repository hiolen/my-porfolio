<section class="about-section smoth" id="about">
    <div class="container">
        <div class="row">
            <div class="section-title wow bounceInUp center animated">
                <h2>About Me</h2><!-- section title -->
            </div>
            <div class="col-lg-6">
                <article class="about-me-box">
                    <h2>A little brief ABOUT Me</h2>
                    <p><strong>Solutions-driven programmer</strong> with a five-year track record of commended performance in modular and object-oriented programming. Well-versed in all phases of the software development lifecycle, with a strong working knowledge of algorithms and data structures. Proven success engineering customized solutions improving business processes, operations and profitability.</p>
                    
                    <a href="{{ asset('files/Resume.pdf') }}"><i class="fa fa-cloud-download" download></i>Download Resume</a>
                </article>
            </div>
            <div class="col-lg-6">
                <div class="skill-box wow bounceInRight center animated">
                    <h2>SKILLS</h2>
                    <p>Laravel Framework</p>
                    <div class="progressbar" data-perc="100">
                        <div class="bar color4"><span></span></div>
                        <div class="label wow bounceInLeft center animated"></div>
                    </div>
                    <p>Django Framework</p>
                    <div class="progressbar" data-perc="85">
                    	<div class="bar color4"><span></span></div>
                    	<div class="label wow bounceInLeft center animated"></div>
                    </div>
                    <p>Adobe Photoshop</p>
                    <div class="progressbar" data-perc="95">
                        <div class="bar"><span></span></div>
                        <div class="label wow bounceInLeft center animated"></div>
                    </div>
                    <p>Adobe Illastrator</p>
                    <div class="progressbar" data-perc="85">
                        <div class="bar color2"><span></span></div>
                        <div class="label wow bounceInLeft center animated"></div>
                    </div>
                    <p>Html & Css</p>
                    <div class="progressbar" data-perc="91">
                        <div class="bar color3"><span></span></div>
                        <div class="label wow bounceInLeft center animated"></div>
                    </div>

                </div><!-- end of /.skill bx -->
            </div><!-- end of /.columns -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of about /.section -->