<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Portfolio - Responsive Resume & Portfolio HTML5 Template</title><!-- site title name -->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="icon" type="image/png" href="images/favicon.png" />
        <!-- start style -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body class="body-box">
        <!-- Preloader -->
        <div id="preloader">
           <span>P</span>
           <span>O</span>
           <span>W</span>
           <span>E</span>
           <span>R</span>
           <span>E</span>
           <span>D </span>
           <span>B</span>
           <span>Y </span>
           <span>D</span>
           <span>E</span>
           <span>E</span>
           <span>D</span>
           <span>E</span>
           <span>L</span>
        </div><!-- end of /.pre loader -->