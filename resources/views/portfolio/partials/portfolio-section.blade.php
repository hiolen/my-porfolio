<section class="portfolio-section" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="section-title portfolio-title wow bounceInUp center animated">
                <h2>MY PORTFOLIO</h2>
                <p>Lets check most recent project I've worked on </p>
            </div>
            <div class="big-demo go-wide" data-js-module="filtering-demo">
                <div class="portfolio-menu">
                    <div class="filter-button-group button-group js-radio-button-group menu-list">
                        <ul class="filter_menu">
                            <li class="active" data-filter="*">All</li>
                            <li data-filter=".web">Web Development</li>
                            <li data-filter=".dashboard">Dashboard</li>
                            <li data-filter=".landing">Landing page</li>
                            <li data-filter=".wordpress">Wordpress</li>
                        </ul>
                    </div>
                </div>

                <div class="portfolio-item gallery">
                    <div class="single-portfolio web wordpress col-md-4 col-sm-6 col-xs-12" data-category="transition">
                        <div class="portfolio-single-item">
                            <a href="images/work-1.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-1.jpg" width="360" height="307" alt="portfolio image 1">
                            </a>
                        </div>
                        <a href="https://www.booklulu.com" class="text-center" target="_blank">
                            <h4>Booklulu</h4>
                        </a>
                    </div>
                    <div class="single-portfolio web landing col-md-4 col-sm-6 col-xs-12" data-category="metalloid">
                        <div class="portfolio-single-item">
                            <a href="images/work-2.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-2.jpg" width="360" height="307" alt="Portfolio image 2">
                            </a>
                        </div>
                        <a href="http://grain-brain.rodelhiolen.com/" class="text-center" target="_blank">
                            <h4>Grain Brain</h4>
                        </a>
                    </div>
                    <div class="single-portfolio web landing col-md-4 col-sm-6 col-xs-12" data-category="transition">
                        <div class="portfolio-single-item">
                            <a href="images/work-3.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-3.jpg" width="360" height="307" alt="Portfolio Image 3">
                            </a>
                        </div>
                        <a href="http://gold.rodelhiolen.com/" class="text-center" target="_blank">
                            <h4>Suzanne Gold</h4>
                        </a>
                    </div>
                    <div class="single-portfolio web landing col-md-4 col-sm-6 col-xs-12" data-category="transition">
                        <div class="portfolio-single-item">
                            <a href="images/work-4.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-4.jpg" width="360" height="307" alt="Portfolio Image 4">
                            </a>
                        </div>
                        <a href="http://gold.rodelhiolen.com/" class="text-center" target="_blank">
                            <h4>Dragon Age</h4>
                        </a>
                    </div>
                    <div class="single-portfolio web dashboard col-md-4 col-sm-6 col-xs-12" data-category="transition">
                        <div class="portfolio-single-item">
                            <a href="images/work-5.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-5.jpg" width="360" height="307" alt=" Portfolio Image 5">
                            </a>
                        </div>
                        <a href="http://deedel.rodelhiolen.com/" class="text-center" target="_blank">
                            <h4>Deedel</h4>
                        </a>
                    </div>
                    <div class="single-portfolio web dashboard col-md-4 col-sm-6 col-xs-12" data-category="transition">
                        <div class="portfolio-single-item">
                            <a href="images/work-6.jpg" data-gal="prettyPhoto[gallery1]">
                                <img src="images/work-6.jpg" width="360" height="307" alt="Portfolio Image 6">
                            </a>
                        </div>
                        <a href="http://leads.rodelhiolen.com/lead" class="text-center" target="_blank">
                            <h4>Data Leads</h4>
                        </a>
                    </div>
                </div>

            </div>
        </div><!-- end of /.row -->
    </div><!-- end of /.container -->
</section><!-- end of /.portfolio section -->