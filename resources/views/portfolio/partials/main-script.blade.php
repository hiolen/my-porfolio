<!-- jquery library -->
        <script src="{{ asset('js/vendor/jquery-1.11.2.min.js') }}"></script>
        <!-- Bootstarp -->
        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
        <!-- parallax -->
        <script src="{{ asset('js/jquery.parallax-1.1.3.js') }}"></script>
        <!-- owl carosuel -->
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <!-- isotope -->
        <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
        <!-- placeholdem -->
        <script src="{{ asset('js/placeholdem.min.js') }}"></script>
        <!-- prettyPhoto -->
        <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
        <!-- switcher -->
        <script src="{{ asset('js/switcher.js') }}"></script>
        <!-- wow js -->
        <script src="{{ asset('js/wow.min.js') }}"></script>
        <!-- custom script -->
        <script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
        <!-- custom script -->
        <script src="{{ asset('js/custom.js') }}"></script>

     </body>
</html>
