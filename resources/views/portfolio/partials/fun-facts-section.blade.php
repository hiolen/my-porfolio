<section class="fun-facts-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="milestone-counter wow swing center animated">
                    <i class="fa fa-check-square-o"></i>
                    <h3 class="stat-count highlight"><b>192</b></h3>
                    <div class="milestone-details">project complate</div>
                </div>
            </div><!-- end of /.columns 1 -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="milestone-counter wow swing center animated">
                    <i class="fa fa-users"></i>
                    <h3 class="stat-count highlight"><b>198</b></h3>
                    <div class="milestone-details">HAPPY CLIENTS </div>
                </div>
            </div><!-- end of /.columns 2 -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="milestone-counter wow swing center animated">
                    <i class="fa fa-coffee"></i>
                    <h3 class="stat-count highlight"><b>950</b></h3>
                    <div class="milestone-details">CUPS OF COFFEE</div>
                </div>
            </div><!-- end of /.columns 3 -->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="milestone-counter wow swing center animated">
                    <i class="fa fa-clock-o"></i>
                    <h3 class="stat-count highlight"><b>650</b></h3>
                    <div class="milestone-details">HOURS OF WORK</div>
                </div>
            </div><!-- end of /.columns 4 -->
        </div><!-- end of /.row -->
    </div><!-- end of /.container  -->
</section><!-- end of /.fun facts section -->