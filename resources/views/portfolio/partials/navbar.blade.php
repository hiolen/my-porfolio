<div class="cover-section smoth" id="home">
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <button class="menu-btn show-btn"></button>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#home"  class="active">Home</a></li>
                            <li><a href="#about">About Me</a></li>
                            <li><a href="#service">My services</a></li>
                            <li><a href="#portfolio">Portfolio</a></li>
                            <li><a id="link" href="#contact">Contact</a></li>
                        </ul><!-- end of /.ul -->
                    </div><!-- end of /.navbar-collapse -->

                </div><!-- end of /.container-fluid -->
            </nav><!-- end of /. nav section -->
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 profile-image wow bounceInUp center animated">
                <img  class="img-responsive" src="images/profile.png" alt="" />
            </div><!-- end of /.column-->
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 profile-item wow flipInX center animated">
                <section class="profile-caption">
                    <h2>Hello, I'm <span>Rodel Hiolen</span></h2>
                    <h2>A <span>Software</span> Engineer Based in <span>PHILIPPINES</span></h2>
                    <div class="social-media">
                        <a href="https://www.facebook.com/rodel.hiolen" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.linkedin.com/in/rodel-hiolen-36316076" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a href="https://plus.google.com/117271402807590018987" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <a href="https://bitbucket.org/hiolen/" target="_blank"><i class="fa fa-bitbucket"></i></a>
                        <a href="https://github.com/H-Freelance-H" target="_blank"><i class="fa fa-github"></i></a>
                    </div>
                </section>
            </div> <!-- end of /.columns -->
        </div><!-- end of /. row  -->
    </div><!-- end of /. cover container  -->
</div><!-- end of /.profile-cover section -->