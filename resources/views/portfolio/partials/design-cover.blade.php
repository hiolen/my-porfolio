<section class="design-cover smoth">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 design-box wow flipInX center animated">
                <figure class="pixel-item">
                    <i class="fa fa-angellist"></i><!-- icon -->
                    <h3>Model</h3><!-- title -->
                    <p>The model is responsible for managing the data of the application. It receives user input from the controller.</p><!-- caption -->
                </figure><!-- end of pixel perfect box -->
            </div><!-- end of /.columns 1 -->
            <div class="col-lg-4 col-md-4  design-box wow flipInX center animated">
                <figure class="clean-design">
                    <i class="fa fa-desktop"></i><!-- icon -->
                    <h3>View</h3><!-- title -->
                    <p>The view means presentation of the model in a particular format.</p><!-- caption -->
                </figure><!-- end of clien design  box -->
            </div><!-- end of /.columns 2 -->
            <div class="col-lg-4 col-md-4  design-box wow flipInX center animated">
                <figure class="pixel-item">
                    <i class="fa fa-paper-plane-o"></i><!-- icon -->
                    <h3>Controller</h3><!-- title -->
                    <p>The controller is responsible for responding to the user input and perform interactions on the data model objects. The controller receives the input, optionally validates the input and then passes the input to the model.</p><!-- caption -->
                </figure><!-- end of pixel perfect box -->
            </div><!-- end of /.columns 3 -->
        </div><!-- end of /.row -->
    </div><!-- end of /.cotainer -->
</section><!-- end of design cover /.section -->