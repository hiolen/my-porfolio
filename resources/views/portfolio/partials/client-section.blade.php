<section class="client-section" id="contact">
    <div class="container">
        <div class="row">
            <div class="section-title wow bounceInUp center animated">
                <h2>GET IN TOUCH</h2>
            </div><!-- contact title -->
            <p id="alert-section"></p>
            <form action="{{ route('page.contact') }}" method="POST" id="formContact">
                {{ csrf_field() }}

                <div class="form-group col-lg-6 wow bounceInUp center animated">
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-user"></i> </div>
                        <input type="text" class="form-control" name="name" id="contact-name" placeholder="Your Name">
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-envelope-o"></i> </div>
                        <input type="text" class="form-control" name="email" id="contact-email" placeholder="Your Email">
                    </div>
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-globe"></i> </div>
                        <input type="text" class="form-control" name="website" id="contact-website" placeholder="Your Website">
                    </div>
                </div>
                <div class="form-group col-lg-6 wow bounceInUp center animated">
                    <div class="input-group">
                        <textarea name="message" class="form-control" id="message" cols="50" placeholder="Your massage"></textarea>
                    </div>
                    <div class="input-group">
                        <button type="submit"  class="form-submit">Send</button>
                    </div>
                </div>
            </form><!-- end of /.contact form -->
        </div><!-- end /.row -->
    </div><!-- end of /.container -->
</section><!-- end of /.client section -->