<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title>Portfolio - Responsive Resume & Portfolio HTML5 Template</title><!-- site title name -->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="icon" type="image/png" href="images/favicon.png" />
        <!-- start style -->
        <link rel="stylesheet" href="css/style.css">
        

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body class="body-box">
       <!-- Preloader -->
        <div id="preloader">
           <span>P</span>
           <span>O</span>
           <span>R</span>
           <span>T</span>
           <span>F</span>
           <span>o</span>
           <span>L</span>
           <span>I</span>
           <span>o</span>
        </div><!-- end of /.pre loader -->

        <div class="wrapper" id="boxscroll">
            @yield('main_content')
        </div><!-- end  of /. wrapper -->

        <!-- jQuery 2.1.4  -->
        <script src="{{ asset('js/jQuery-2.1.4.min.js') }}"></script>
        <!-- Bootstarp -->
        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
        <!-- parallax -->
        <script src="{{ asset('js/jquery.parallax-1.1.3.js') }}"></script>
        <!-- owl carosuel -->
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <!-- isotope -->
        <script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
        <!-- placeholdem -->
        <script src="{{ asset('js/placeholdem.min.js') }}"></script>
        <!-- prettyPhoto -->
        <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
        <!-- switcher -->
        <script src="{{ asset('js/switcher.js') }}"></script>
        <!-- wow js -->
        <script src="{{ asset('js/wow.min.js') }}"></script>
        <!-- custom script -->
        <script src="{{ asset('js/jquery.nicescroll.min.js') }}"></script>
        <!-- custom script -->
        <script src="{{ asset('js/custom.js') }}"></script>
        <!-- Jquery Validation -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

        @yield('extra_script')
     </body>
</html>
