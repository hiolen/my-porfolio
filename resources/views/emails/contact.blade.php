<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
	<h4><strong>Dear</strong> Rodel,</h4>

	<p>{!! $text !!}</p>

	<br><br><br><br>
	<p><strong>Truly yours,</strong></p>
	<p>{{ $name }} | {{ $email }}</p>
</body>
</html>